<?php

require_once(sprintf("%s/../vendor/autoload.php", __DIR__));
require_once(sprintf("%s/../src/Loader.php", __DIR__));

use Symfony\Component\HttpFoundation\Request;

// Load Netwatcher source

Netwatcher\Loader::load();

// Load configuration

$yaml = new Symfony\Component\Yaml\Parser();

$config = $yaml->parse(file_get_contents(
    Netwatcher\Utils::joinPath(__DIR__, '..', 'conf', 'base.yml')
));

// Build Silex app and register services

$app = new Silex\Application();

// Debug mode

$app['debug'] = $config["debug"];

// URL Generator service

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Translation service 

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale' => 'es', 
    'locale_fallbacks' => array('en')
));

// Twig service

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => sprintf("%s/../views", __DIR__),
));

$assetUrl = function (Twig_Environment $env, $asset) use ($config) {
    return Netwatcher\Utils::joinPath($config["rootpath"], 'static', $asset);
};

$app['twig']->addFilter(new Twig_SimpleFilter(
    'asseturl', $assetUrl, array('needs_environment' => true)
));

///////////////////////
// Routes definition //
///////////////////////

$app->get('/', 'Netwatcher\\Controllers::index')->bind('index');
$app->get('/marketing', 'Netwatcher\\Controllers::marketing')->bind('marketing');
$app->post('/marketing/generate', 'Netwatcher\\Controllers::marketingGenerate')->bind('marketing_generate');
$app->get('/engineering/smart', 'Netwatcher\\Controllers::engineeringSmart')->bind('engineering_smart');
$app->get('/engineering/graphs', 'Netwatcher\\Controllers::engineeringGraphs')->bind('engineering_graphs');
$app->get('/command', 'Netwatcher\\Controllers::command')->bind('command');

// Run app

$app->run();

?>