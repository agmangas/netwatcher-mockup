$(document).ready(function() {

    // Initialize global variables

    var charts = {};
    var graphIds = [];
    var graphMetrics = {};

    // Generates the ID of a graph's container div

    function generateGraphId(elementValue) {
        var graphId = sprintf('%s-%s', elementValue.toLowerCase(), parseInt(Math.random() * 1e10));

        return graphId;
    }

    // Generates the HTML of a graph's container div

    function generateGraphDiv(graphId) {
        var graphDiv = sprintf('<div id="%s" class="graphs-graph"></div>', graphId);

        return graphDiv;
    }

    // Initializes the HTML code of a graph panel

    function initializeGraphHtml(graphId, elementValue, elementName) {
        var graphContent = '';

        graphContent = graphContent + sprintf(
            '<div class="droppable-graph panel panel-info" data-graph-id="%s" data-element="%s">',
            graphId, elementValue
        );

        graphContent = graphContent + sprintf(
            '<div class="panel-heading">' +
            '<div class="row">' +
            '<div class="col-xs-6 text-left">' +
            '<i class="fa fa-bar-chart-o"></i> &nbsp; %s' +
            '</div>' +
            '<div class="col-xs-6 text-right">' +
            '<i class="graph-close fa fa-times"></i>' +
            '</div>' +
            '</div>' +
            '</div>',
            elementName
        );

        graphContent = graphContent + sprintf(
            '<div class="panel-body"><div class="graphs-placeholder well well-lg remove-bottom-margin">' +
            '<span class="text-info">Gráfico vacio</span> &nbsp; Arrastra algún KPI para visualizarlo</div></div>'
        );

        graphContent = graphContent + sprintf(
            '<div class="panel-footer">' +
            '<div class="row">' +
            '<div class="col-md-6">' +
            '<div class="input-group">' +
            '<span class="input-group-addon input-sm">Desde</span>' +
            '<input id="options-to" class="form-control input-sm" type="text" disabled="disabled" value="2014-02-20 00:00:00">' +
            '</div>' +
            '</div>' +
            '<div class="col-md-6">' +
            '<div class="input-group">' +
            '<span class="input-group-addon input-sm">Hasta</span>' +
            '<input id="options-to" class="form-control input-sm" type="text" disabled="disabled" value="2014-02-20 23:59:00">' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        return graphContent;
    }

    // Paints KPI on graph

    function paintKpi(elementValue, elementName, metricValue, metricName, graphId) {
        if (!graphMetrics[graphId])
            graphMetrics[graphId] = [];

        if (graphMetrics[graphId].indexOf(metricValue) > -1)
            return;

        graphMetrics[graphId].push(metricValue);

        var typeDefault = 'spline';

        var typeByMetric = {
            'drop_calls_percent': 'bar'
        };

        var tooltipDefault = {
            valueDecimals: 0,
            valueSuffix: ' llam.',
            xDateFormat: '%A, %b %e, %H:%M:%S'
        };

        var tooltipByMetric = {
            'drop_calls_percent': {
                valueDecimals: 0,
                valueSuffix: ' %',
                xDateFormat: '%A, %b %e, %H:%M:%S'
            }
        };

        $.getJSON(TEST_DATA_URL, function(data) {
            var series = {
                data: [],
                type: (typeByMetric[metricValue]) ? typeByMetric[metricValue] : typeDefault,
                tooltip: (tooltipByMetric[metricValue]) ? tooltipByMetric[metricValue] : tooltipDefault,
                name: metricName
            };

            $.each(data[elementValue][metricValue], function(idx, val) {
                series.data.push([val[0] * 1000, val[1]]);
            });

            charts[graphId].addSeries(series);
        });
    }

    // Removes placeholder from graph if necessary

    function removePlaceholderAndInitChart(graphId) {
        var $graphPanel = $(sprintf('.droppable-graph[data-graph-id=%s]', graphId));
        var $graphsPlaceholder = $graphPanel.find('.graphs-placeholder');

        if ($graphsPlaceholder.length) {
            $graphsPlaceholder.remove();
            var graphDiv = generateGraphDiv(graphId);
            $graphPanel.find('.panel-body').html(graphDiv);
            initChart(graphId);
        }
    }

    // Create new graph

    function createNewGraph()  {

        // Build HTML content

        var elementValue = $('#element-selectpicker').val();
        var elementName = $('#element-selectpicker').find(":selected").text();
        var graphId = generateGraphId(elementValue);
        graphIds.push(graphId);
        var graphHtml = initializeGraphHtml(graphId, elementValue, elementName);
        $('#graphs-help').hide();
        $('#graphs-result').append(graphHtml);

        // Clean droppable behavior

        if ($('.droppable-graph').data('draggable'))
            $('.droppable-graph').droppable('destroy');

        // Attach dropable behavior to KPIs

        $('.droppable-graph').droppable({
            drop: function(event, ui) {
                var graphId = $(this).attr('data-graph-id');
                removePlaceholderAndInitChart(graphId);
                var metricValue = ui.draggable.attr('data-metric');
                var metricName = ui.draggable.text();
                paintKpi(elementValue, elementName, metricValue, metricName, graphId);
            },
            over: function(event, ui) {
                $(this).addClass('light-draggable-panel');
            },
            out: function(event, ui) {
                $(this).removeClass('light-draggable-panel');
            },
            deactivate: function(event, ui) {
                $(this).removeClass('light-draggable-panel');
            }
        });

        // Return Graph ID

        return graphId;
    }

    // Removes a graph

    function removeGraph(graphId) {
        if (charts[graphId]) {
            charts[graphId].destroy();
            delete charts[graphId];
            delete graphMetrics[graphId];
        }

        var graphIdIdx = graphIds.indexOf(graphId);
        if (graphIdIdx > -1) {
            graphIds.splice(graphIdIdx, 1);
        }
    }

    // Initializes graph

    function initChart(graphId) {
        charts[graphId] = new Highcharts.Chart({
            chart: {
                renderTo: graphId,
                zoomType: 'x',
                animation: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: null
                },
                min: 0
            }
        });
    }

    // Get URL parameter

    $.urlParam = function(name) {
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        } else {
            return results[1] || 0;
        }
    }

    // Create graph on init

    function graphOnInit() {
        var params = {}

        params.metricValue = $.urlParam('metric');
        params.elemenValue = $.urlParam('element');

        if (params.metricValue == null || params.elemenValue == null)
            return;

        params.metricName = $(sprintf('.kpi-option[data-metric=%s]', params.metricValue)).text();
        params.elemenName = $(sprintf('option.element-option[data-element=%s]', params.elemenValue)).text();

        if (!params.metricName || !params.elemenName)
            return;

        var graphId = createNewGraph();

        removePlaceholderAndInitChart(graphId);

        paintKpi(params.elemenValue, params.elemenName, params.metricValue, params.metricName, graphId);
    }

    // Enable Bootstrap Select

    $('#element-selectpicker').selectpicker({
        style: "btn-info"
    });

    // Enable draggable behavior

    $('.draggable-metrics a').draggable({
        revert: true,
        scroll: true,
        revertDuration: 0
    });

    // Delete graph handler

    $('#graphs-result').on('click', '.graph-close', function() {
        var graphId = $(this).parents('.droppable-graph').attr('data-graph-id');
        removeGraph(graphId);
        $(sprintf('.droppable-graph[data-graph-id=%s]', graphId)).remove();
        if (!graphIds.length)
            $('#graphs-help').show();
    });

    // New graph button handler

    $('#new-graph').click(function() {
        createNewGraph();
    });

    graphOnInit();

});
