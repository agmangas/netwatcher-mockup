$(document).ready(function() {

    // Enable Bootstrap Select

    $('#incidence-selectpicker, #location-selectpicker, #order-selectpicker').selectpicker({
        style: "btn-info"
    });

    // Dropdown checkbox list

    $('.dropdown-menu').on('click', function(e) {
        if ($(this).hasClass('dropdown-menu-form')) {
            e.stopPropagation();
        }
    });

    // Sub-tech list update

    function toggleTechList($clicked) {
        $('.sub-tech-list').hide();
        $(sprintf('.sub-tech-list[data-tech-option=%s]', $clicked.attr("id"))).show();
    }

    $('.tech-option').change(function() {
        toggleTechList($(this));
    });

    // Initialize map

    var center = [40.41, -3.69];

    var centerLatLng = new google.maps.LatLng(center[0], center[1]);

    var map = new google.maps.Map(
        $('#location-map-canvas')[0], {
            center: centerLatLng,
            zoom: 9,
            mapTypeControl: false,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP]
            }
        }
    );

    var radiusCircle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        center: centerLatLng,
        radius: 10000
    });

    var marker = new google.maps.Marker({
        position: centerLatLng,
        map: map
    });


});
