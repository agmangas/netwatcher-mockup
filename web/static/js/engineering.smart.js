$(document).ready(function() {

    var chart = null;

    var center = [40.41, -3.69];

    var centerLatLng = new google.maps.LatLng(center[0], center[1]);

    var map = new google.maps.Map(
        $('#options-map')[0], {
            center: centerLatLng,
            zoom: 9,
            mapTypeControl: false,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP]
            }
        }
    );

    var marker = new google.maps.Marker({
        position: centerLatLng,
        map: map
    });

    $.getJSON(TEST_DATA_URL, function(data) {
        var series = [];

        $.each(data["MAD001"]["drop_calls_poor_coverage"], function(idx, val) {
            series.push([val[0] * 1000, val[1]]);
        });

        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'smart-result-graph-01',
                zoomType: 'x',
                animation: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Cantidad'
                },
                min: 0
            },
            series: [{
                name: 'Drop Calls Poor Coverage',
                data: series,
                type: 'spline',
                tooltip: {
                    valueDecimals: 0,
                    valueSuffix: ' llam.',
                    xDateFormat: '%A, %b %e, %H:%M:%S'
                }
            }]
        });
    });

});
