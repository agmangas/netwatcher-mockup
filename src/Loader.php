<?php

namespace Netwatcher {

    class Loader {

        public static function load() {
            require_once(sprintf("%s/Controllers.php", __DIR__));
            require_once(sprintf("%s/Utils.php", __DIR__));
        }
    }
}

?>