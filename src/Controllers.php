<?php

namespace Netwatcher {

    use Symfony\Component\HttpFoundation\Request;
    use Silex\Application;
    use Netwatcher\Utils;

    class Controllers {

        public function index(Request $request, Application $app) {
            return $app['twig']->render('index.html.twig', array(
                'menu' => 'index'
            ));
        }

        public function marketing(Request $request, Application $app) {
            return $app['twig']->render('marketing.html.twig', array(
                'menu' => 'marketing'
            ));
        }

        public function marketingGenerate(Request $request, Application $app) {
            $downFormat = $request->get("down-format", null);

            if(!isset($downFormat))
                $app->abort(404, "Invalid format");

            switch($downFormat) {
                case "csv":
                    list($contentType, $extension) = array("text/csv", "csv");
                    break;

                case "xml":
                    list($contentType, $extension) = array("application/xml", "xml");
                    break;
            }

            $file = Utils::joinPath(__DIR__, '..', 'web', 'static', 'marketing', sprintf('data.%s', $extension));

            $stream = function () use ($file) { 
                readfile($file);
            };

            return $app->stream($stream, 200, array(
                'Content-Type' => $contentType,
                'Content-length' => filesize($file), 
                'Content-Disposition' => sprintf('attachment; filename="netwatcher-marketing.%s"', $extension)
            ));
        }

        public function engineeringSmart(Request $request, Application $app) {
            return $app['twig']->render('engineering.smart.html.twig', array(
                'menu' => 'engineering'
            ));
        }

        public function engineeringGraphs(Request $request, Application $app) {
            return $app['twig']->render('engineering.graphs.html.twig', array(
                'menu' => 'engineering'
            ));
        }

        public function command(Request $request, Application $app) {
            return $app['twig']->render('command.html.twig', array(
                'menu' => 'command'
            ));
        }
    }
}

?>